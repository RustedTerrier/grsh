use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdout;
use std::io::BufReader;
use std::path::Path;

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    for arg in 1..args.len() {
        match args[arg].as_str() {
            "--help" => {
                println!("Usage: cat [OPTIONS] FILE");
                println!("Reads a file");
            }
            arg => {
                if Path::new(arg).exists() {
                    let file = File::open(arg)?;
                    let mut buf_reader = BufReader::new(file);
                    let mut contents = String::new();
                    buf_reader.read_to_string(&mut contents)?;
                    print!("{}", contents);
                    stdout().flush().unwrap();
                }
            }
        }
    }
    Ok(())
}
