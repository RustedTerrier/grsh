use std::env;
use std::fs;
use std::path::Path;

fn main() {
    let mut verbose_flag = false;
    let mut remove_dirs = false;
    let args: Vec<String> = env::args().collect();
    for arg in 1..args.len() {
        match args[arg].as_str() {
            "--help" => {
                println!("Usage: rm [OPTIONS] DIRECTORY");
                println!("Removes files or directories");
                return;
            }
            "-v" | "--verbose" => {
                verbose_flag = true;
            }
            "-d" | "--dir" => {
                remove_dirs = true;
            }
            arg => {
                if Path::new(arg).exists() {
                    if !Path::new(arg).is_dir() {
                        fs::remove_file(arg.to_string());
                        if verbose_flag {
                            println!("rm: removed '{}'", arg);
                        }
                    } else if Path::new(arg).is_dir() && remove_dirs {
                        fs::remove_dir(arg.to_string());
                        if verbose_flag {
                            println!("rm: removed directory '{}'", arg);
                        }
                    } else if Path::new(arg).is_dir() {
                        println!("rm: cannot remove '{}': Is a directory", arg);
                    }
                } else {
                    println!("rm: cannot remove '{}': No such file or directory", arg);
                }
            }
        };
    }
}
