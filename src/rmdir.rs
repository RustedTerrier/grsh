use std::env;
use std::fs;

fn main() {
    let mut verbose_flag = false;
    let args: Vec<String> = env::args().collect();
    for arg in 1..args.len() {
        match args[arg].as_str() {
            "--help" => {
                println!("Usage: rmdir [OPTIONS] DIRECTORY");
                println!("Removes empty directory");
                return;
            }
            "-v" | "--verbose" => {
                verbose_flag = true;
            }
            arg => {
                fs::remove_dir(arg.to_string());
                if verbose_flag {
                    println!("rmdir: removed directory '{}'", arg);
                }
            }
        };
    }
}
