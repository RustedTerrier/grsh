# TODO

## Basic shell features

- [x] Pipelines `|`, done thanks to that [article](https://www.joshmcguigan.com/blog/build-your-own-shell-rust/)
- [ ] `;`
- [ ] AND (`&&`)
- [ ] OR (`||`)
- [ ] > and >>
- [ ] status handling

## Commands

### Builtins

- [x] `cd` - only basics done, need to make arguments
- [x] `exit`
- [x] `:`
- [x] `true`
- [x] `false`
- [x] `pwd`
- [ ] `echo` - basics and some vars done, need to do a lot of other things 
- [ ] `type`
- [ ] `command`
- [ ] `alias`
- [ ] `export`
- [ ] and a lot of other

### Not Builtins

- [ ] `mkdir` - only basics, `--help` and `--verbose`
- [ ] `rmdir` - only basics, `--help` and `--verbose`
- [ ] `ls` - just listing dirs and `--help`, `-a` flags, still need fixes about current dir
- [ ] `rm` 
- [ ] `touch`
- [ ] `cat`
- [ ] `tac`
- [ ] `grep`
- [ ] `cp`
- [ ] `mv`
- [ ] `head`
- [ ] `tail`
- [x] `yes`
- [ ] `ed`
